$(document).ready(function(){	
	
	var owl = $('.slider_1');
	  owl.owlCarousel({
		margin: 1,
		nav: true,
		dots: true,
		items: 3,
		loop: true,
		responsive: {
		  0: {
			items: 1,
			nav: false,
			dots: true
		  },
		  420: {
			items: 1,
			nav: true,
			dots: true
		  },
		  730: {
			items: 2
		  },
		  991: {
			items: 3
		  }
		}
	})
	
	var owl = $('.person_slider');
	  owl.owlCarousel({
		margin: 10,
		nav: true,
		dots: false,
		items: 10,
		loop: true,
		responsive: {
		  0: {
			items: 2,
			nav: false,
			dots: true
		  },
		  390: {
			items: 3,
			nav: true,
			dots: false
		  },
		  540: {
			items: 4
		  },
		  620: {
			items: 5
		  },
		  991: {
			items: 6
		  },
		  1220: {
			items: 8
		  },
		  1540: {
			items: 10
		  }
		}
	})
	
	var owl = $('.bps');
	  owl.owlCarousel({
		margin: 40,
		nav: true,
		dots: true,
		items: 3,
		loop: true,
		responsive: {
		  0: {
			items: 1,
			nav: false,
			dots: true
		  },
		  480: {
			items: 2,
			nav: true,
			dots: true
		  },
		  670: {
			items: 3
		  },
		  790: {
			items: 2
		  },
		  991: {
			items: 3
		  }
		}
	})
	
	/*$('#rating_1').rating({
		fx: 'float',
		image: 'images/stars.png',
		loader: 'images/ajax-loader.gif',
		width: 19,
		minimal: 0.6,
		url: 'rating.php'
	});	 */
	
	$('.m_filter_btn').click(function(){ 
		if($(this).hasClass('active')){
			$('.other_filter').slideUp(300);
			$(this).removeClass('active');
		} else {
			$('.other_filter').slideDown(300);
			$(this).addClass('active');
		}
	});

	$('.nav_filter_btn').click(function(){ 
		if($(this).hasClass('active')){
			$('nav').removeClass('full_show');
			$(this).removeClass('active');
		} else {
			$('nav').addClass('full_show');
			$(this).addClass('active');
		}
	});
	

	$('.nav_btn').click(function(){ 
		if($('.top_nav ul').hasClass('active')){
			$('.top_nav ul').removeClass('active');
			$('.top_nav ul').slideUp(200);
		} else {
			$('.top_nav ul').addClass('active');
			$('.top_nav ul').slideDown(200);
		}
	});
	
	$("body").click(function (event) {	
		var w = $(window).width();
		if (w<=560){
			if ($(event.target).closest(".top_nav").length === 0) {
				$('.top_nav ul').slideUp(200);	
			}
		}
	});
	
});
