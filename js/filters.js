function select_kfc(_id){
	document.getElementById('kfc_select_0').innerHTML=document.getElementById('kfc_select_'+_id).innerHTML;
	document.getElementById('kfc_select_block').style.display = 'none';
	document.getElementById('kfc_select_hidden_block').style.display = 'none';
}
function header_kfc(){
	if(document.getElementById('kfc_select_block').style.display == 'block') {
		document.getElementById('kfc_select_block').style.display = 'none';
		document.getElementById('kfc_select_hidden_block').style.display = 'none';
	}
	else 
	{
		document.getElementById('kfc_select_block').style.display = 'block';
		document.getElementById('kfc_select_hidden_block').style.display = 'block';
	}
}
function load_kfc(){
	document.getElementById('kfc_select_0').innerHTML=document.getElementById('kfc_select_1').innerHTML;
}
function filtertag(_id){
	if(document.getElementById('kfc_tag_'+_id).checked){
		document.getElementById('kfc_tag_label_'+_id).style.color = "#393939";
		document.getElementById('kfc_tag_label_'+_id).style.background = "#fff";
	}
	else
	{
		document.getElementById('kfc_tag_label_'+_id).style.color = "#fff";
		document.getElementById('kfc_tag_label_'+_id).style.background = "#393939";
	}
}
$(document).ready(function(){   
		$(window).scroll(function () {
			if ($(this).scrollTop() > 0) {
				$('#scroller').fadeIn();
			} else {
				$('#scroller').fadeOut();
			}
		});
		$('#scroller').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 400);
			return false;
		});
	});
	function edit_currency(_id){
		var cur=document.getElementById('currency_val_'+_id).innerHTML;
			if(cur == '<s>р</s>') document.getElementById('edit_currency_pub_'+_id).style.border = '2px dotted #A5D094;';
			if(cur == '<s>г</s>') document.getElementById('edit_currency_uah_'+_id).style.border = '2px dotted #A5D094;';
			if(cur == '<s>б</s>') document.getElementById('edit_currency_bel_'+_id).style.border = '2px dotted #A5D094;';
			if(document.getElementById('edit_currency_'+_id).style.display == 'block') {
			document.getElementById('edit_currency_'+_id).style.display = 'none';
			}
			else 
			{
			document.getElementById('edit_currency_'+_id).style.display = 'block';
			}
	}
	function currency_accept(_id, _val){
		var cur=document.getElementById('currency_val_'+_id).innerHTML;
			if(_val == 1) document.getElementById('edit_currency_pub_'+_id).innerHTML;
			if(_val == 2) document.getElementById('edit_currency_uah_'+_id).innerHTML;
			if(_val == 3) document.getElementById('edit_currency_bel_'+_id).innerHTML;
			document.getElementById('edit_currency_'+_id).style.display = 'none';
	}